//
//  ViewController.swift
//  HTMLFormatTextView
//
//  Created by 謝佳瑋 on 2018/8/23.
//  Copyright © 2018年 secure. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setTextView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setTextView() {
        let liData = [
            "Lorem Ipsum is simply dummy text of the printing and typesetting",
            "industry. Lorem Ipsum has been the industry's standard dummy",
            "text ever since the 1500s, when an unknown printer took a galley",
            "of type and scrambled it to make a type specimen book. It has",
            "survived not only five centuries, but also the leap into electronic"
        ]
        let liList = liData.map{String.htmlTagValue($0, tag: "li")}.joined()
        let data = [
            "title": "HTML Format TextView",
            "author": "Jason",
            "loremText": "Contrary to popular belief",
            "noticeItmList": liList
        ]
        if let path = URL.htmlPath(fileName: "test") {
            let htmlString = try? String(contentsOf: path, encoding: .utf8)
            
            if let html = htmlString {
                textView.attributedText = String.htmlFormat(from: html.htmlRender(data))
            }
        }
    }

}

